// const Deck = require('./model/Deck');
const Blackjack = require('./model/Blackjack');

// const deckService = new Deck();
const blackjack = new Blackjack(10);

const games = {
  dealer: 0,
  player: 0,
  tie: 0,
};

for (let i = 0; i < 80; i += 1) {
  const result = blackjack.simulateGame();
  console.log(result);
  games[result.result] += 1;
}

console.log(games);
