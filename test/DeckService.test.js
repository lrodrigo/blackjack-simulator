const test = require('colored-tape');

const Deck = require('../model/Deck');

const deckService = new Deck();
const cardsPerDeck = 52;

test('Constructor works', async (t) => {
  const deck = deckService.createDeck();

  t.plan(2);
  t.equal(typeof deck, 'object');
  t.equal(deck.length, cardsPerDeck);
});

test('Deck has cards', async (t) => {
  deckService.createDeck();
  const hasCards = deckService.hasCards();

  t.plan(2);
  t.equal(typeof hasCards, 'boolean');
  t.equal(true, hasCards);
});

test('A random card is found exactly once in the deck', async (t) => {
  const randomCard = deckService.getRandomCard();

  let timesFound = 0;

  while (deckService.hasCards()) {
    const card = deckService.dealCards(1);
    if (JSON.stringify(card[0]) === JSON.stringify(randomCard)) {
      timesFound += 1;
    }
  }

  t.plan(2);
  t.equal(typeof timesFound, 'number');
  t.equal(timesFound, 1);
});

test('Adding a deck works', async (t) => {
  deckService.createDeck();
  deckService.addDeck();

  t.plan(2);
  t.equal(deckService.getCards().length, cardsPerDeck * 2);
  t.equal(deckService.getNumberOfDecks(), 2);
});

test('Adding a shuffled deck works', async (t) => {
  deckService.createDeck();
  deckService.addShuffledDeck();

  t.plan(2);
  t.equal(deckService.getCards().length, cardsPerDeck * 2);
  t.equal(deckService.getNumberOfDecks(), 2);
});

test('Dealing too many cards throw error', async (t) => {
  try {
    for (;;) {
      deckService.dealCards(1);
    }
  } catch (e) {
    t.plan(2);
    t.equal(typeof e.message, 'string');
    t.equal(e.message, 'Can\'t deal 1 cards, only 0 left in deck');
  }
});
