const test = require('colored-tape');

const Blackjack = require('../model/Blackjack');
const Deck = require('../model/Deck');
const IntegerHelper = require('../helper/IntegerHelper');

const numberOfDecks = 2;
const blackjack = new Blackjack(numberOfDecks);

test('Constructor works', async (t) => {
  const cardsPerDeck = 52;

  const decks = blackjack.getDecks();
  t.plan(3);
  t.equal(typeof blackjack, 'object');
  t.equal(typeof decks, 'object');
  t.equal(decks.length, cardsPerDeck * numberOfDecks);
});

test('Score is 21 when cards are ace, 6, 4', async (t) => {
  const cards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 6 },
    { suit: 'diamonds', rank: 4 },
  ];

  const score = blackjack.calculateScore(cards);

  t.plan(2);
  t.equal(typeof score, 'number');
  t.equal(score, 21);
});

test('Score is 21 when cards are ace, ace, ace and 8', async (t) => {
  const cards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 'ace' },
    { suit: 'diamonds', rank: 'ace' },
    { suit: 'diamonds', rank: 8 },
  ];

  const score = blackjack.calculateScore(cards);

  t.plan(2);
  t.equal(typeof score, 'number');
  t.equal(score, 21);
});

test('Score is 21 when cards are 7, 7 and 7', async (t) => {
  const cards = [
    { suit: 'hearts', rank: 7 },
    { suit: 'clubs', rank: 7 },
    { suit: 'diamonds', rank: 7 },
  ];

  const score = blackjack.calculateScore(cards);

  t.plan(2);
  t.equal(typeof score, 'number');
  t.equal(score, 21);
});

test('Score is 14 when cards are ace and 3', async (t) => {
  const cards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 3 },
  ];

  const score = blackjack.calculateScore(cards);

  t.plan(2);
  t.equal(typeof score, 'number');
  t.equal(score, 14);
});

test('Score is blackjack when cards are ace and king', async (t) => {
  const cards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 'king' },
  ];

  const score = blackjack.calculateScore(cards);

  t.plan(2);
  t.equal(typeof score, 'string');
  t.equal(score, 'blackjack');
});

test('Delar wins with 20 agains 19', async (t) => {
  const dealerCards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 9 },
  ];

  const playerCards = [
    { suit: 'hearts', rank: 9 },
    { suit: 'clubs', rank: 'king' },
  ];

  const winner = blackjack.getWinner(dealerCards, playerCards);

  t.plan(2);
  t.equal(typeof winner, 'string');
  t.equal(winner, 'dealer');
});

test('Player wins with 19 agains 17', async (t) => {
  const playerCards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 8 },
  ];

  const dealerCards = [
    { suit: 'hearts', rank: 7 },
    { suit: 'clubs', rank: 'king' },
  ];

  const winner = blackjack.getWinner(dealerCards, playerCards);

  t.plan(2);
  t.equal(typeof winner, 'string');
  t.equal(winner, 'player');
});

test('Tie with cards with same rank', async (t) => {
  const playerCards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'hearts', rank: 8 },
  ];

  const dealerCards = [
    { suit: 'clubs', rank: 'ace' },
    { suit: 'clubs', rank: 8 },
  ];

  const winner = blackjack.getWinner(dealerCards, playerCards);

  t.plan(2);
  t.equal(typeof winner, 'string');
  t.equal(winner, 'tie');
});

test('Player wins with blackjack agains 17', async (t) => {
  const playerCards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 'queen' },
  ];

  const dealerCards = [
    { suit: 'hearts', rank: 7 },
    { suit: 'clubs', rank: 'king' },
  ];

  const winner = blackjack.getWinner(dealerCards, playerCards);

  t.plan(2);
  t.equal(typeof winner, 'string');
  t.equal(winner, 'player');
});

test('Dealer wins with blackjack agains 21', async (t) => {
  const playerCards = [
    { suit: 'hearts', rank: 7 },
    { suit: 'clubs', rank: 7 },
    { suit: 'diamonds', rank: 7 },
  ];

  const dealerCards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 'king' },
  ];

  const winner = blackjack.getWinner(dealerCards, playerCards);

  t.plan(2);
  t.equal(typeof winner, 'string');
  t.equal(winner, 'dealer');
});

test('Tie with two blackjacks', async (t) => {
  const playerCards = [
    { suit: 'clubs', rank: 'ace' },
    { suit: 'clubs', rank: 'queen' },
  ];

  const dealerCards = [
    { suit: 'hearts', rank: 'ace' },
    { suit: 'clubs', rank: 'king' },
  ];

  const winner = blackjack.getWinner(dealerCards, playerCards);

  t.plan(2);
  t.equal(typeof winner, 'string');
  t.equal(winner, 'tie');
});

test('Test simulate a game works', async (t) => {
  const gameResult = blackjack.simulateGame();
  const potentialResults = ['dealer', 'player', 'tie'];

  t.plan(2);
  t.equal(typeof gameResult, 'object');
  t.ok(potentialResults.indexOf(gameResult.result) !== -1, `Game result is ${gameResult.result}`);
});

test('Test basic strategy should stand when player has blackjack', async (t) => {
  const ranks = Object.values(Deck.getRanks());
  const suits = Object.values(Deck.getSuits());

  const playerCards = [
    { suit: 'clubs', rank: 'ace' },
    { suit: 'clubs', rank: 'queen' },
  ];

  const randomDealerCard = {
    suit: suits[Math.floor(Math.random() * suits.length)],
    rank: ranks[Math.floor(Math.random() * ranks.length)],
  };

  const nextMove = Blackjack.basicStrategy(randomDealerCard, blackjack.calculateScore(playerCards));

  t.plan(2);
  t.equal(typeof nextMove, 'string');
  t.equal(nextMove, 'stand');
});

test('Test basic strategy should stand when player has 17-20', async (t) => {
  const ranks = Object.values(Deck.getRanks());
  const suits = Object.values(Deck.getSuits());

  const numberBetween17And20 = IntegerHelper.randomBetween(17, 20);

  /*
  const randomPlayerCard = {
    suit: suits[Math.floor(Math.random() * suits.length)],
    rank: numberBetween17And20,
  }
  */

  const randomDealerCard = {
    suit: suits[Math.floor(Math.random() * suits.length)],
    rank: ranks[Math.floor(Math.random() * ranks.length)],
  };

  const nextMove = Blackjack.basicStrategy(randomDealerCard, numberBetween17And20);

  t.plan(2);
  t.equal(typeof nextMove, 'string');
  t.equal(nextMove, 'stand');
});

test('Test basic strategy should stand when player has 12 and dealer 6', async (t) => {
  const suits = Object.values(Deck.getSuits());

  const playerScore = 12;

  const randomDealerCard = {
    suit: suits[Math.floor(Math.random() * suits.length)],
    rank: 6,
  };

  const nextMove = Blackjack.basicStrategy(randomDealerCard, playerScore);

  t.plan(2);
  t.equal(typeof nextMove, 'string');
  t.equal(nextMove, 'stand');
});

test('Test basic strategy should hit when player has 12 and dealer 2', async (t) => {
  const suits = Object.values(Deck.getSuits());

  const playerScore = 12;

  const randomDealerCard = {
    suit: suits[Math.floor(Math.random() * suits.length)],
    rank: 2,
  };

  const nextMove = Blackjack.basicStrategy(randomDealerCard, playerScore);

  t.plan(2);
  t.equal(typeof nextMove, 'string');
  t.equal(nextMove, 'hit');
});

test('Test basic strategy should hit when player has 5-8', async (t) => {
  const suits = Object.values(Deck.getSuits());
  const ranks = Object.values(Deck.getRanks());

  const numberBetween5And8 = IntegerHelper.randomBetween(5, 8);

  const randomDealerCard = {
    suit: suits[Math.floor(Math.random() * suits.length)],
    rank: ranks[Math.floor(Math.random() * ranks.length)],
  };

  const nextMove = Blackjack.basicStrategy(randomDealerCard, numberBetween5And8);

  t.plan(2);
  t.equal(typeof nextMove, 'string');
  t.equal(nextMove, 'hit');
});
