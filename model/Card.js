
class Card {
  constructor(suit, rank) {
    this.suit = suit;
    this.rank = rank;
  }

  getCard() {
    return {
      suit: this.suit,
      rank: this.rank,
    };
  }
}

module.exports = Card;
