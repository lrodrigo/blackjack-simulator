const Deck = require('./Deck');


class Blackjack {
  constructor(numberOfDecks) {
    this.suits = Deck.getSuits();
    this.ranks = Deck.getRanks();

    this.decks = [];
    const deck = new Deck();
    for (let i = 0; i < numberOfDecks; i += 1) {
      const newDeck = deck.createShuffledDeck();
      this.decks = this.decks.concat(newDeck);
    }
  }

  getDecks() {
    return this.decks;
  }

  calculateScore(cards) {
    let score = 0;
    let aces = 0;

    for (let i = 0; i < cards.length; i += 1) {
      if (cards[i].rank === this.ranks.ACE) {
        aces += 1;
      }
      score += this.getCardPoints(cards[i]);
    }

    // Check if it's blackjack
    if (score === 21 && cards.length === 2) {
      return 'blackjack';
    }

    while (score > 21 && aces > 0) {
      score -= 10;
      aces -= 1;
    }

    return score;
  }

  getCardPoints(card) {
    if (card.rank === this.ranks.ACE) {
      return 11;
    }

    if (card.rank === this.ranks.JACK || card.rank === this.ranks.QUEEN
      || card.rank === this.ranks.KING) {
      return 10;
    }

    return card.rank;
  }

  getWinner(dealerCards, playerCards) {
    // Player's Blackjack
    if (this.calculateScore(playerCards) === 'blackjack') {
      if (this.calculateScore(dealerCards) === 'blackjack') {
        return 'tie';
      }
      return 'player';
    }

    // Dealer's Blackjack
    if (this.calculateScore(dealerCards) === 'blackjack') {
      if (this.calculateScore(playerCards) === 'blackjack') {
        return 'tie';
      }
      return 'dealer';
    }

    if (this.calculateScore(dealerCards) > 21) {
      return 'player';
    }

    if (this.calculateScore(playerCards) > 21) {
      return 'dealer';
    }

    if (this.calculateScore(dealerCards) > this.calculateScore(playerCards)) {
      return 'dealer';
    }

    if (this.calculateScore(dealerCards) < this.calculateScore(playerCards)) {
      return 'player';
    }

    return 'tie';
  }

  dealCards(numberOfCards) {
    const cards = [];

    if (numberOfCards > this.decks.length) {
      throw Error(`Can't deal ${numberOfCards} cards, only ${this.decks.length} left in deck`);
    }

    for (let i = 0; i < numberOfCards; i += 1) {
      cards.push(this.decks.pop());
    }

    // console.log(`${this.decks.length} cards left`);

    return cards;
  }

  simulateGame() {
    // console.log(deck);

    // const cards = deckService.dealCards(2);
    // console.log(cards);
    // console.log('Score', deckService.calculateScore(cards));

    let dealer = this.dealCards(1);
    let player = this.dealCards(2);

    // console.log('Dealer', dealer);
    // console.log('Player', player);

    /*
    // Hit only once
    while (this.calculateScore(player) <= 15) {
      player = player.concat(this.dealCards(1));
    }
    */
    let playerScore = this.calculateScore(player);

    while (Blackjack.basicStrategy(dealer, playerScore) === 'hit') {
      player = player.concat(this.dealCards(1));
      playerScore = this.calculateScore(player);
    }

    // Busted!
    if (playerScore !== 'blackjack' && playerScore > 21) {
      return {
        dealer,
        player,
        result: 'dealer',
      };
    }

    // console.log('Player', player);

    // Hit until 17
    while (this.calculateScore(dealer) < 17) {
      dealer = dealer.concat(this.dealCards(1));
    }

    // console.log('Dealer', dealer);

    return {
      dealer,
      player,
      result: this.getWinner(dealer, player),
    };
  }

  static basicStrategy(dealerCard, playerScore) {
    // Busted
    if (playerScore > 21) {
      return 'stand';
    }

    // Blackjack
    if (playerScore === 'blackjack') {
      return 'stand';
    }
    // const playerScore = this.calculateScore(playerCards);
    if (playerScore >= 17 && playerScore <= 20) {
      return 'stand';
    }

    if (playerScore >= 13 && playerScore <= 16) {
      if (Number.isInteger(dealerCard.rank) && dealerCard.rank <= 6) {
        return 'stand';
      }

      if (!Number.isInteger(dealerCard.rank) || dealerCard.rank >= 7) {
        return 'hit';
      }

      throw Error('Invalid 1');
    }

    if (playerScore === 12) {
      if (Number.isInteger(dealerCard.rank) && dealerCard.rank >= 4 && dealerCard.rank <= 6) {
        return 'stand';
      }

      return 'hit';
    }

    return 'hit';
    /*
    switch (dealerCard) {
      case 2:
        break;
      case 3:
        break;
      case 4:
        break;
      case 5:
        break;
      case 6:
        break;
      case 7:
        break;
      case 8:
        break;
      case 9:
        break;
      case 'jack': case 'queen': case 'king':
        break;
      case 'ace':
        break;
      default:
        throw Error(`${dealerCard} is invalid.`);
    }
    */
  }
}

module.exports = Blackjack;
