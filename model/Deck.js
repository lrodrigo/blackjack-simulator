
class Deck {
  constructor() {
    this.suits = Deck.getSuits();
    this.ranks = Deck.getRanks();

    this.cards = [];
    this.usedCards = [];
    this.numberOfDecks = 0;
  }

  static getSuits() {
    return {
      CLUBS: 'clubs',
      DIAMONDS: 'diamonds',
      HEARTS: 'hearts',
      SPADES: 'spades',
    };
  }

  static getRanks() {
    return {
      ACE: 'ace',
      JACK: 'jack',
      QUEEN: 'queen',
      KING: 'king',
    };
  }

  getCards() {
    return this.cards;
  }

  getNumberOfDecks() {
    return this.numberOfDecks;
  }

  hasCards() {
    return this.cards.length > 0;
  }

  createShuffledDeck() {
    return Deck.shuffleArray(this.createDeck());
  }

  createDeck() {
    this.cards = [];
    this.numberOfDecks = 0;

    return this.addDeck();
  }

  addShuffledDeck() {
    this.addDeck();
    return this.shuffleDeck();
  }

  addDeck() {
    const suits = [
      this.suits.CLUBS,
      this.suits.DIAMONDS,
      this.suits.HEARTS,
      this.suits.SPADES,
    ];
    const ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10,
      this.ranks.JACK, this.ranks.QUEEN, this.ranks.KING, this.ranks.ACE];

    const suitsSize = ranks.length;
    const suitsQty = suits.length;

    const newDeck = [];

    for (let i = 0; i < suitsQty; i += 1) {
      for (let j = 0; j < suitsSize; j += 1) {
        newDeck.push({
          suit: suits[i],
          rank: ranks[j],
        });
      }
    }

    this.cards = this.cards.concat(newDeck);

    this.numberOfDecks += 1;

    return this.cards;
  }

  shuffleDeck() {
    this.cards = Deck.shuffleArray(this.cards);
    return this.cards;
  }

  dealCards(numberOfCards) {
    const cards = [];

    if (numberOfCards > this.cards.length) {
      throw Error(`Can't deal ${numberOfCards} cards, only ${this.cards.length} left in deck`);
    }

    for (let i = 0; i < Math.min(numberOfCards, this.cards.length); i += 1) {
      const card = this.cards.pop();
      this.usedCards.push(card);
      cards.push(card);
    }

    // console.log(`${this.cards.length} cards left`);

    return cards;
  }

  getRandomCard() {
    const suits = [
      this.suits.CLUBS,
      this.suits.DIAMONDS,
      this.suits.HEARTS,
      this.suits.SPADES,
    ];
    const ranks = [2, 3, 4, 5, 6, 7, 8, 9, 10,
      this.ranks.JACK, this.ranks.QUEEN, this.ranks.KING, this.ranks.ACE];

    return {
      suit: suits[Math.floor(Math.random() * suits.length)],
      rank: ranks[Math.floor(Math.random() * ranks.length)],
    };
  }

  static shuffleArray(a) {
    const arrayCopy = a;
    const arrayLength = arrayCopy.length;

    for (let i = arrayLength - 1; i > 0; i -= 1) {
      const j = Math.floor(Math.random() * (i + 1));
      [arrayCopy[i], arrayCopy[j]] = [arrayCopy[j], arrayCopy[i]];
    }

    return arrayCopy;
  }
}

module.exports = Deck;
